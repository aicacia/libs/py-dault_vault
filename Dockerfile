FROM python:3.8.1

RUN apt update && apt upgrade -y
RUN apt autoremove -y && apt autoclean && apt clean
RUN apt install -y libpq-dev

WORKDIR /app
COPY ./requirements.txt /app

RUN pip install -r requirements.txt

ENV PYTHONPATH /app
COPY . /app
RUN chmod +x /app/entrypoint.sh

ENTRYPOINT [ "/app/entrypoint.sh" ]