## Data Vault

### Getting Started

```bash
# runs all tests
python setup.py test
```

### Docker/Helm

```bash
docker build -t registry.aicacia.com/service/data_vault:0.1.0 .
docker run -it --rm registry.aicacia.com/service/data_vault:0.1.0
```

#### Database

```bash
# create a postgres docker container
python setup.py postgres
# deletes postgres docker container
python setup.py postgres_delete
```

More information at
https://sqlalchemy-migrate.readthedocs.io/en/latest/

```bash
# init db version control
python ./data_vault_repo/manage.py version_control
# migrate
python ./data_vault_repo/manage.py upgrade
# rollback
python ./data_vault_repo/manage.py downgrade 0
```
