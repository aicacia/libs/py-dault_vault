import pytest
from sqlalchemy.exc import IntegrityError
from data_vault.model.hub import Hub
from data_vault.model.hub_connection import HubConnection
from data_vault.model.create_hub_class import create_hub_class, generate_hash_id_for
from data_vault.service.hub_connection import read_hub_connection, read_hub_connection_to_pandas, read_hub_connection_from_pandas
from data_vault.session import Session


def test_insert_hub_class():
    hub_id = Session().query(Hub).filter_by(name="data_vault_hub").one().id
    hub_connection = Session().query(HubConnection).filter_by(hub_id=hub_id).one()

    hub_rows = list()
    session = Session()
    for hub_row in read_hub_connection(hub_connection):
        session.add(hub_row)
        session.commit()
        session.refresh(hub_row)
        hub_rows.append(hub_row)
        assert generate_hash_id_for(
            hub_row, ["id"]) == hub_row.data_vault_hub_hash_id

    with pytest.raises(IntegrityError):
        error_session = Session()
        HubTest = create_hub_class(hub_connection.hub)
        error_session.add(HubTest(id=hub_id).generate_hash_id())
        error_session.commit()

    for hub_row in hub_rows:
        session.delete(hub_row)
        session.commit()


def test_read_hub_connection_to_pandas():
    hub_id = Session().query(Hub).filter_by(name="data_vault_hub").one().id
    hub_connection = Session().query(HubConnection).filter_by(hub_id=hub_id).one()

    data_frame = read_hub_connection_to_pandas(hub_connection)

    rows = []
    for row in read_hub_connection_from_pandas(hub_connection, data_frame):
        rows.append(row)
        assert generate_hash_id_for(row, ["id"]) == row.data_vault_hub_hash_id

    assert len(rows) == 1
