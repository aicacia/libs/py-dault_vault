from setuptools import find_packages, setup
from distutils.cmd import Command
import os

name = "data_vault"
organization = "aicacia"
version = "0.1.0"
registry = "registry.aicacia.com"


def read_file(filename: str):
    with open(filename, "r") as fp:
        data = fp.read()
    return data


class PostgresCommand(Command):
    description = 'creates a local postgres docker instance'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system("mkdir -p ${PWD}/.volumes/" + name + "-postgres")
        os.system("docker run --rm -d --name " + name +
                  "-postgres -e POSTGRES_PASSWORD=postgres -p 5432:5432 -v ${PWD}/.volumes/" + name + "-postgres:/var/lib/postgresql/data postgres:12")


class PostgresDeleteCommand(Command):
    description = 'deletes the local postgres docker instance'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system("docker rm -f " + name + "-postgres")
        os.system("sudo rm -rf ${PWD}/.volumes/" + name + "-postgres")


requirements = []
test_requirements = ["pytest"]

setup(
    name=name,
    version=version,
    author="Nathan Faucett",
    author_email="nathanfaucett@gmail.com",
    description="A data vault for python applications",
    long_description=read_file("./README.md"),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/aicacia/py-dault_vault",
    license="MIT",
    packages=find_packages(),
    setup_requires=["pytest-runner"],
    install_requires=requirements,
    tests_require=test_requirements,
    classifiers=[
        "Programming Language :: Python :: 3",
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ],
    python_requires=">= 3.6",
    zip_safe=False,
    cmdclass={
        "postgres": PostgresCommand,
        "postgres_delete": PostgresDeleteCommand
    }
)
