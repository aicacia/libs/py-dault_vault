import gc
from sqlalchemy import MetaData, Column, String, Boolean, Integer, ForeignKey, Table, Enum, UniqueConstraint, DateTime, func
from data_vault.session import Session
from data_vault.model.hub import Hub
from data_vault.model.hub_field import HubField, HubFieldKind
from data_vault.model.hub_connection import HubConnection, HubConnectionKind
from data_vault.model.hub_connection_migration import HubConnectionMigration
from data_vault.model.connection import Connection
from data_vault.model.cron_job import CronJob
from data_vault.service.hub import create_hub, delete_hub
from data_vault.service.hub_connection import create_hub_connection, migrate_hub_connection, delete_hub_table
from data_vault.service.connection import create_connection, delete_connection

metadata = MetaData()

hubs = Table(
    Hub.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String, nullable=False),
    Column('inserted_at', DateTime, default=func.now()),
    UniqueConstraint('name')
)
hub_fields = Table(
    HubField.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('hub_id', Integer, ForeignKey('hubs.id')),
    Column('name', String, nullable=False),
    Column('kind', Enum(HubFieldKind), nullable=False),
    Column('nullable', Boolean, nullable=True),
    Column('default', String, nullable=True),
    Column('inserted_at', DateTime, default=func.now()),
    UniqueConstraint('hub_id', 'name')
)
connections = Table(
    Connection.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String, nullable=False),
    Column('url', String, nullable=False),
    Column('inserted_at', DateTime, default=func.now()),
    UniqueConstraint('name', 'url')
)
hub_connections = Table(
    HubConnection.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('hub_id', Integer, ForeignKey('hubs.id')),
    Column('connection_id', Integer, ForeignKey('connections.id')),
    Column('kind', Enum(HubConnectionKind), nullable=False),
    Column('table', String, nullable=False),
    Column('inserted_at', DateTime, default=func.now())
)
hub_connection_migrations = Table(
    HubConnectionMigration.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('hub_connection_id', Integer, ForeignKey('hub_connections.id')),
    Column('inserted_at', DateTime, default=func.now()),
    UniqueConstraint('hub_connection_id')
)
cron_jobs = Table(
    CronJob.__tablename__,
    metadata,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('name', String, nullable=False),
    Column('expr', String, default="0 * * * *", nullable=False),
    UniqueConstraint('name')
)


def upgrade(migrate_engine):
    metadata.bind = migrate_engine
    hubs.create()
    hub_fields.create()
    connections.create()
    hub_connections.create()
    hub_connection_migrations.create()
    cron_jobs.create()

    # Seeds
    data_vault_connection = create_connection(Connection(name="data_vault",
                                                         url="postgresql://postgres:postgres@localhost:5432"))

    data_vault_hub = create_hub(Hub(
        name="data_vault_hub",
        fields=[HubField(
            name="id",
            kind=HubFieldKind.integer, nullable=False
        )]
    ))
    create_hub_connection(HubConnection(
        data_vault_hub.id, data_vault_connection.id, 'hubs'))


def downgrade(migrate_engine):
    metadata.bind = migrate_engine
    session = Session(bind=migrate_engine)

    data_vault_connection_engine = session.query(
        Connection).filter_by(name="data_vault").one().engine()

    data_vault_hub_id = session.query(
        Hub).filter_by(name="data_vault_hub").one().id

    delete_hub_table(data_vault_connection_engine, data_vault_hub_id)

    # force the session to be collected to avoid db lock in drop table
    session.close()
    session = None
    gc.collect()

    cron_jobs.drop()
    hub_connection_migrations.drop()
    hub_connections.drop()
    hub_fields.drop()
    hubs.drop()
    connections.drop()
