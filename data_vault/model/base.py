from datetime import datetime
from sqlalchemy import func, Column, DateTime
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class TimestampMixin(object):
    inserted_at = Column(DateTime, default=func.now())
