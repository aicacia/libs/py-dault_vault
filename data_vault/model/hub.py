from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from data_vault.model.base import Base, TimestampMixin
from data_vault.model.hub_field import HubField


class Hub(Base, TimestampMixin):
    __tablename__ = 'hubs'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    fields = relationship(
        'HubField', order_by=HubField.id, back_populates='hub', cascade="all, delete, delete-orphan, expunge")

    def __init__(self, id=None, name='', fields=list()):
        self.id = id
        self.name = name
        self.fields = fields

    def __repr__(self):
        return "<Hub (id='%s', name='%s', fields='%s', inserted_at='%s')>" % (self.id, self.name, self.fields, self.inserted_at)
