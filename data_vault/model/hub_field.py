import enum
from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Enum
from sqlalchemy.orm import relationship
from data_vault.model.base import Base, TimestampMixin


class HubFieldKind(enum.Enum):
    integer = 'integer'
    boolean = 'boolean'
    string = 'string'

    def to_sqlalchemy(self):
        if self == HubFieldKind.integer:
            return Integer
        elif self == HubFieldKind.boolean:
            return Boolean
        elif self == HubFieldKind.string:
            return String


class HubField(Base, TimestampMixin):
    __tablename__ = 'hub_fields'

    id = Column(Integer, primary_key=True, nullable=False)
    hub_id = Column(Integer, ForeignKey('hubs.id'))
    name = Column(String, nullable=False)
    kind = Column(Enum(HubFieldKind), nullable=False)
    nullable = Column(Boolean, nullable=True)
    default = Column(String, nullable=True)
    hub = relationship('Hub', back_populates='fields',
                       cascade="all, delete, delete-orphan, expunge", single_parent=True)

    def __init__(self, id=None, hub_id=None, name='', kind=HubFieldKind.string, nullable=True, default=None):
        self.id = id
        self.hub_id = hub_id
        self.name = name
        self.kind = kind
        self.nullable = nullable
        self.default = default

    def __repr__(self):
        return "<HubField (id='%s', hub_id='%s', name='%s', kind='%s', nullable='%s', default='%s', inserted_at='%s')>" % (self.id, self.hub_id, self.name, self.kind, self.nullable, self.default, self.inserted_at)
