import enum
from sqlalchemy import Column, Integer, String, ForeignKey, Enum
from sqlalchemy.orm import relationship
from data_vault.model.base import Base, TimestampMixin


class HubConnectionKind(enum.Enum):
    create = 'create'
    delete = 'delete'


class HubConnection(Base, TimestampMixin):
    __tablename__ = 'hub_connections'

    id = Column(Integer, primary_key=True, nullable=False)
    hub_id = Column(Integer, ForeignKey('hubs.id'))
    hub = relationship('Hub', single_parent=True)
    connection_id = Column(Integer, ForeignKey('connections.id'))
    connection = relationship(
        'Connection', cascade="all, delete, delete-orphan, expunge", single_parent=True)
    kind = Column(Enum(HubConnectionKind), nullable=False)
    table = Column(String, nullable=False)

    def __init__(self, hub_id, connection_id, table, id=None, kind=HubConnectionKind.create):
        self.id = id
        self.hub_id = hub_id
        self.connection_id = connection_id
        self.kind = kind
        self.table = table

    def __repr__(self):
        return "<HubConnection (id='%s', hub_id='%s', connection_id='%s', kind='%s', table='%s', inserted_at='%s')>" % (self.id, self.hub_id, self.connection_id, self.kind, self.table, self.inserted_at)
