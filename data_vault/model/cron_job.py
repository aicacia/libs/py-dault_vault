from sqlalchemy import Column, Integer, String
from data_vault.model.base import Base, TimestampMixin


class CronJob(Base, TimestampMixin):
    __tablename__ = 'cron_jobs'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    expr = Column(String, default="0 * * * *", nullable=False)

    def __init__(self, id=None, name='Every Hour', expr='0 * * * *'):
        self.id = id
        self.name = name
        self.expr = expr

    def __repr__(self):
        return "<Connection (id='%s', name='%s' expr='%s')>" % (self.id, self.name, self.expr)
