from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from data_vault.model.base import Base, TimestampMixin


class HubConnectionMigration(Base, TimestampMixin):
    __tablename__ = 'hub_connection_migrations'

    id = Column(Integer, primary_key=True, nullable=False)
    hub_connection_id = Column(Integer, ForeignKey('hub_connections.id'))
    hub_connection = relationship('HubConnection', single_parent=True)

    def __init__(self, id=None, hub_connection_id=None):
        self.id = id
        self.hub_connection_id = hub_connection_id

    def __repr__(self):
        return "<HubConnectionMigration (id='%s', hub_connection_id='%s', inserted_at='%s')>" % (self.id, self.hub_connection_id, self.inserted_at)
