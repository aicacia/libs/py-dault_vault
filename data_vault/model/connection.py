from sqlalchemy import Column, Integer, String, create_engine
from data_vault.model.base import Base, TimestampMixin
from data_vault.session import Session


class Connection(Base, TimestampMixin):
    __tablename__ = 'connections'

    id = Column(Integer, primary_key=True, nullable=False)
    name = Column(String, nullable=False)
    url = Column(String, nullable=False)

    def __init__(self, id=None, name='memory', url='sqlite://'):
        self.id = id
        self.name = name
        self.url = url

    def __repr__(self):
        return "<Connection (id='%s', name='%s' url='%s')>" % (self.id, self.name, self.url)

    def engine(self):
        return create_engine(self.url)

    def session(self):
        return Session(bind=self.engine())
