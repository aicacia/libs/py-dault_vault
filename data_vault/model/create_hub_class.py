from typing import Set
import re
import hashlib
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from data_vault.model.base import Base, TimestampMixin
from data_vault.model.hub import Hub


def create_hub_class(hub: Hub, table=None):
    hub_name = hub.name
    if table == None:
        table = hub_name + "s"
    hash_id_name = get_hash_id_name(hub_name)
    class_name = snake_to_camel(hub_name)
    # TODO: do something better than adding an s
    field_names = set()
    all_columns = set([hash_id_name, "inserted_at"])
    fields = dict(__tablename__=table,
                  __table_args__={'keep_existing': True})

    for field in hub.fields:
        fields[field.name] = Column(
            field.kind.to_sqlalchemy(), nullable=field.nullable)
        field_names.add(field.name)
        all_columns.add(field.name)

    def __repr__(self):
        values = ""

        for field_name in all_columns:
            values += field_name + "='" + str(getattr(self, field_name)) + "', "

        return "<" + class_name + "(" + values + ")>"

    def copy(self, other):
        for field_name in all_columns:
            setattr(self, field_name, getattr(other, field_name))
        return self

    def columns(self):
        return all_columns

    def values(self):
        values = []
        for field_name in all_columns:
            values.append(getattr(self, field_name))
        return values

    def generate_hash_id(self):
        setattr(self, hash_id_name, generate_hash_id_for(self, field_names))
        return self

    def get_hash_id(self):
        return getattr(self, hash_id_name)

    fields['__repr__'] = __repr__
    fields['copy'] = copy
    fields['columns'] = columns
    fields['all_columns'] = all_columns
    fields['values'] = values
    fields['generate_hash_id'] = generate_hash_id
    fields['get_hash_id'] = get_hash_id
    fields[hash_id_name] = Column(
        String, primary_key=True, nullable=False)

    Class = type(class_name, (Base, TimestampMixin), fields)
    return Class


def generate_hash_id_for(obj: object, attr_names: Set[str] = set()):
    hash = hashlib.sha256()

    for attr_name in attr_names:
        hash.update(str(getattr(obj, attr_name)).encode())

    return hash.hexdigest()


def get_hash_id_name(hub_name: str):
    return "%s_hash_id" % hub_name


def snake_to_camel(string):
    return "".join(x.capitalize() or "_" for x in string.split("_"))
