from data_vault.session import Session
from data_vault.model.connection import Connection


def create_connection(connection: Connection):
    session = Session()
    try:
        session.add(connection)
        session.commit()
        session.refresh(connection)
        return connection
    except:
        session.rollback()
        raise


def delete_connection(id: int):
    session = Session()
    try:
        connection = session.query(Connection).get(id)
        session.delete(connection)
        session.commit()
        return connection
    except:
        session.rollback()
        raise


def get_connection(id: int):
    return Session().query(Connection).get(id)
