from data_vault.session import Session
from data_vault.model.hub_connection_migration import HubConnectionMigration


def create_hub_connection_migration(hub_connection_id: int):
    session = Session()
    try:
        hub_connection_migration = HubConnectionMigration(
            hub_connection_id=hub_connection_id)
        session.add(hub_connection_migration)
        session.commit()
        session.refresh(hub_connection_migration)
        return hub_connection_migration
    except:
        session.rollback()
        raise


def delete_hub_connection_migration(id: int):
    session = Session()
    try:
        hub_connection_migration = session.query(
            HubConnectionMigration).get(id)
        session.delete(hub_connection_migration)
        session.commit()
        return hub_connection_migration
    except:
        session.rollback()
        raise
