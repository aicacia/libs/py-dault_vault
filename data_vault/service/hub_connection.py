from typing import Any
import pandas
from fastparquet import ParquetFile
from sqlalchemy import func, MetaData
from sqlalchemy.engine import Engine
from data_vault.session import Session
from data_vault.model.create_hub_class import create_hub_class
from data_vault.model.hub import Hub
from data_vault.model.hub_connection import HubConnection, HubConnectionKind
from data_vault.model.hub_connection_migration import HubConnectionMigration
from data_vault.service.hub import get_hub
from data_vault.service.hub_connection_migration import create_hub_connection_migration


def create_hub_connection(hub_connection: HubConnection):
    session = Session()
    try:
        last_hub_connection = session.query(HubConnection).filter(
            HubConnection.id == session.query(func.max(HubConnection.id))).one_or_none()

        if last_hub_connection != None and last_hub_connection.kind == hub_connection.kind:
            raise Exception("last HubConnection is of kind %s" %
                            last_hub_connection.kind)

        session.add(hub_connection)
        session.commit()
        session.refresh(hub_connection)

        migrate_hub_connection(hub_connection)

        return hub_connection
    except:
        session.rollback()
        raise


def delete_hub_connection(id: int):
    session = Session()
    try:
        hub_connection = session.query(HubConnection).get(id)
        session.delete(hub_connection)
        session.commit()
        return hub_connection
    except:
        session.rollback()
        raise


def get_hub_connection(id: int):
    return Session().query(HubConnection).get(id)


def get_hub_connection_migration_by_hub_connection_id(hub_connection_id: int):
    return Session().query(HubConnectionMigration).filter_by(hub_connection_id=hub_connection_id).one_or_none()


def read_hub_connection(hub_connection: HubConnection, count: int = 1000):
    connection = hub_connection.connection
    session = connection.session()

    SourceHubClass = create_hub_class(hub_connection.hub, hub_connection.table)
    SyncHubClass = create_hub_class(hub_connection.hub)

    metadata = MetaData()
    metadata.bind = session.bind

    SourceHubClass.__table__.metadata = metadata

    for row in session.query(SourceHubClass).yield_per(count):
        row.generate_hash_id()
        sync_row = SyncHubClass().copy(row)
        yield sync_row


def read_hub_connection_to_pandas(hub_connection: HubConnection, count: int = 1000):
    data = {}
    for row in read_hub_connection(hub_connection, count):
        values = row.values()
        for index, column in enumerate(row.columns()):
            data.setdefault(column, []).append(values[index])
    return pandas.DataFrame(data)


def read_hub_connection_from_pandas(hub_connection: HubConnection, data_frame: pandas.DataFrame):
    HubClass = create_hub_class(hub_connection.hub)
    columns = HubClass.all_columns

    for _index, row in data_frame.iterrows():
        out_row = HubClass()
        for column in columns:
            setattr(out_row, column, row[column])
        yield out_row


def migrate_hub_connections():
    for hub_connection in Session().query(HubConnection).all():
        migrate_hub_connection(hub_connection)


def migrate_hub_connection(hub_connection: HubConnection):
    if get_hub_connection_migration_by_hub_connection_id(hub_connection.id) != None:
        return create_hub_class(hub_connection.hub)

    HubClass = None

    if hub_connection.kind == HubConnectionKind.create:
        HubClass = create_hub_table(hub_connection.connection.engine(),
                                    hub_connection.hub_id)
    elif hub_connection.kind == HubConnectionKind.delete:
        HubClass = delete_hub_table(hub_connection.connection.engine(),
                                    hub_connection.hub_id)

    create_hub_connection_migration(hub_connection.id)

    return HubClass


def create_hub_table(engine: Engine, hub_id: int):
    hub = get_hub(hub_id)
    HubClass = create_hub_class(hub)

    metadata = MetaData()
    metadata.bind = engine

    HubClass.__table__.metadata = metadata
    HubClass.__table__.create()

    return HubClass


def delete_hub_table(engine: Engine, hub_id: int):
    hub = get_hub(hub_id)
    HubClass = create_hub_class(hub)

    metadata = MetaData()
    metadata.bind = engine

    HubClass.__table__.metadata = metadata
    HubClass.__table__.drop()

    return HubClass
