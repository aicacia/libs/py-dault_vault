from data_vault.session import Session
from data_vault.model.hub import Hub


def create_hub(hub: Hub):
    session = Session()
    try:
        session.add(hub)
        session.commit()
        session.refresh(hub)
        return hub
    except:
        session.rollback()
        raise


def delete_hub(id: int):
    session = Session()
    try:
        hub = session.query(Hub).get(id)
        session.delete(hub)
        session.commit()
        return hub
    except:
        session.rollback()
        raise


def get_hub(id: int):
    return Session().query(Hub).get(id)
