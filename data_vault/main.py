from prefect import task, Flow


@task
def main_task():
    print("Hello, world!")


with Flow("Data Vault") as flow:
    main_task()


def main():
    flow.run()
